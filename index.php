<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your 2nd Look - Order</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-theme4.css">
</head>
<body>
    <div class="form-body">
        <div class="website-logo">
            <a href="index.php">
                <div class="logo">
                    <img class="logo-size" src="images/header_logo.png" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
				<div> <p>Only radiologists who are <strong>AΩA</strong> (Alpha Omega Alpha radiology award graduates - the top 10%), <strong><em>Phi Beta Kappa</em></strong>, or graduates with exceptional distinction will interpret and read your report and provide a second opinion.<br>
                          <br>
                          YOUR 2ND LOOK's radiologists and cardiologists are the cream of the crop.<br>
                          So whether you live in New York City, Carson City, in the heartland, or anywhere in the world, you now have the opportunity to have the very finest radiology and cardiology interpretation available in the US at a very fair price. <br>
                          <br>
                          The "Your 2nd Look" radiologist will be a specialist in the area of your imaging study. Once your images have been analyzed by our award-winning radiologist, you will receive a very comprehensive report in 24-48 hours. We do not have time pressures that can sometimes be applied to other radiologists to complete a study interpretation. If we need more than 48 hours to get it right, we will do that and notify you. Nothing is more important than getting it right.<br>
                          <br>
                        This report can then be presented to your medical specialist for review and recommendation.</p></div>
                <div class="info-holder">
                    <img src="images/graphic1.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
					
                    <div class="form-items">
                        
                       
                        <div class="page-links">
                            <a href="index.php" class="active">Order</a><a href="login.php">Login</a>
                        </div>
                        <form>
                            <input class="form-control" type="text" name="username" placeholder="E-mail Address *" required>
                            <input class="form-control" type="password" name="password" placeholder="Password *" required>
                            <input class="form-control" type="password" name="password" placeholder="Confirm Password *" required>
							<input class="form-control" type="text" name="name" placeholder="Full Name *" required>
							<input class="form-control" type="text" name="name" placeholder="Street Address *" required>
							<input class="form-control" type="text" name="name" placeholder="City *" required>
							<div class="form-group">
							 
							  <select class="form-control">
								<option value="AL">Select City *</option>
								<option value="AL">AL</option>
								<option value="AR">AR</option>
								<option value="AZ">AZ</option>
								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DC">DC</option>
								<option value="DE">DE</option>
								<option value="FL">FL</option>
								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="IA">IA</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>
								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="MA">MA</option>
								<option value="MD">MD</option>
								<option value="ME">ME</option>
								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MO">MO</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="NC">NC</option>
								<option value="ND">ND</option>
								<option value="NE">NE</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NV">NV</option>
								<option value="NY">NY</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="PR">PR</option>
								<option value="RI">RI</option>
								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VA">VA</option>
								<option value="VT">VT</option>
								<option value="WA">WA</option>
								<option value="WI">WI</option>
								<option value="WV">WV</option>
								<option value="WY">WY</option>
							  </select>
							</div>
							<input class="form-control" type="text" name="name" placeholder="Zip *" required>
							<input class="form-control" type="text" name="name" placeholder="Phone *" required>
                            <div class="form-button">
							<div><strong>Select Exam</strong></div>
								
                               <div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="x-ray" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="x-ray">X-RAY - $69.00</label>
							</div>
								
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="ct" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="ct">CT - $159.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="cta" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="cta">CTA - $249.00</label>
							</div>
								
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="mri" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="mri">MRI - $159.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="mri-breast" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="mri-breast">MRI - Breast - $199.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="mri-cardio" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="mri-cardio"> MRI - Cardio - $199.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="us" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="us"> US - $109.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="mammogram" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="mammogram"> Mammogram - $129.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="pet-ct" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="pet-ct"> PET CT - $299.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="nuc-med" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="nuc-med"> Nuc Med - $149.00</label>
							</div>
								
							<div class="custom-control custom-radio custom-control-inline">
							  <input type="radio" id="echo" name="customRadioInline1" class="custom-control-input">
							  <label class="custom-control-label" for="echo"> Echo - $99.00</label>
							</div>
								
								
								
								
								
								
                            </div>
							
							<div><strong>Select Payment Method</strong></div>
							<div class="custom-control custom-radio custom-control-inline">
  <input type="radio" id="paypal" name="customRadioInline2" class="custom-control-input">
  <label class="custom-control-label" for="paypal">Paypal</label>
</div>
<div class="custom-control custom-radio custom-control-inline">
  <input type="radio" id="card" name="customRadioInline2" class="custom-control-input">
  <label class="custom-control-label" for="card">Credit Card</label>
</div>
							
							
							
							
							<div class="terms form-button">
                                <div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="customCheck1">
  <label class="custom-control-label" for="customCheck1">I acknowledge that I have read, understand and accept the <a href="#" data-toggle="modal" data-target="#largeShoes">terms and conditions</a> above. </label>
</div>
                            </div>
							
							
							<div class="form-button">
                                <button id="submit" type="submit" class="ibtn center">SUBMIT ORDER</button> 
								
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
	
<!-- The modal -->
<div class="modal fade" id="largeShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">

<div class="modal-header">
<h4 class="modal-title" id="modalLabelLarge">Terms and Condition</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>

<div class="modal-body">
<p>I am choosing to have my imaging study read by a Your 2nd Look LLC licensed US radiologist or US cardiologist for the sole purpose of reviewing the image and providing a report based thereon. I will not be receiving treatment advice from the company or its radiologists and cardiologists and must still seek the advice of my primary healthcare provider. </p>

<p>I agree to hold the company harmless for any treatment rendered or not rendered by my provider based on the report issued by the company.</p>

<p>I am paying Your 2nd Look LLC for the services provided in accordance with the posted fee schedule and subject to any applicable discounts offered by the company.  I understand that it is likely that this payment will not be reimbursed by Medicare, Medicaid or any private insurance plan and will be a personal out of pocket expense. </p>

<p>I understand that Your 2nd Look operates in full compliance with HIPAA requirements concerning patient privacy and that its website conforms to applicable security and privacy standards. </p>

<p><strong>REFUND POLICY:</strong> The company reserves the right to not issue a report on images which it feels have observable flaws or defects. In such case, I will be issued a full refund for payment made and notified of the problem with the image.</p>

<p>By checking the box below I will proceed to the payment portion of the website to place and complete my order. 
					</p></div>

</div>
</div>
</div>	
	
	
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>