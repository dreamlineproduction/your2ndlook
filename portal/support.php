<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Your 2nd Look - Support</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
		
		<!-- Dropzone css -->
         <link href="../plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" />
		
		<!-- Sweet Alert css -->
        <link href="../plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>

    <body>
		
		<?php include 'include/header.php';?>

        


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                           
                            <h4 class="page-title">Customer Support</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
<div class="row text-center">
                    <div class="col-sm-6 col-lg-6 col-xl-4">
                        <div class="card-box widget-flat border-custom bg-custom text-white">
                            <i class="fi-tag"></i>
                            <h3 class="m-b-10">35</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Total Support Sent</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-xl-4">
                        <div class="card-box bg-warning widget-flat border-warning text-white">
                            <i class="fi-archive"></i>
                            <h3 class="m-b-10">10</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Pending Support</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-xl-4">
                        <div class="card-box widget-flat border-success bg-success text-white">
                            <i class="fi-help"></i>
                            <h3 class="m-b-10">25</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Solved Support</p>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            
                            <p class="text-muted m-b-30 font-13">
                                Enter the information bellow and we will get back to you as soon as possible.
                            </p>

                            <form>
                                
								
                                <div class="form-group">
                                    <label for="subject" class="col-form-label">Subject</label>
                                    <input type="text" class="form-control" id="subject" placeholder="Enter subject">
                                </div>
								
								<div class="form-group">
                                    <label for="subject" class="col-form-label">Details</label>
                                    <textarea class="form-control" rows="10" placeholder="enter your details"></textarea>
                                </div>
                                
                                
                               <button type="button" class="btn btn-primary" id="sa-support">SUBMIT</button>
                               
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2018-19 © Your 2nd Look
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
<!-- Modal -->
<div class="modal fade" id="comments" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Enter Comments</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <textarea class="form-control" rows="5" placeholder="Enter any comments if you want to share with this image"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
		
<!-- Modal -->
<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-box">
                            <h4 class="header-title m-t-0">File Uploader</h4>
                            <p class="text-muted font-14 m-b-10">
                                You can upload miltiple files at once.
                            </p>

                            <form action="#" class="dropzone" id="dropzone">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>

                            </form>
                            <div class="clearfix text-right mt-3">
                                <button type="button" class="btn btn-custom waves-effect waves-light">Submit</button>
                            </div>

                        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		
		<!-- Dropzone js -->
        <script src="../plugins/dropzone/dropzone.js"></script>
		
		<!-- Sweet Alert Js  -->
        <script src="../plugins/sweet-alert/sweetalert2.min.js"></script>
        <script src="assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>