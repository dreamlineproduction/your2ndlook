<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Your 2nd Look - User Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
		
		
		
		<!-- DataTables -->
        <link href="../plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
		
		

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>

    <body>
		
		<?php include 'include/admin-header.php';?>

        


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                           
                            <h4 class="page-title">User Management</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title">Manage Users</h4>

                            


                            <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%" id="datatable">
                                <thead>
                                <tr>
                                    <th>
                                        ID
                                    </th>
                                    <th>Customer Name</th>
                                    <th>Email</th>
                                    <th>Exam</th>
                                    <th>Total Files</th>
                                    <th>Joining Date</th>
                                    <th class="hidden-sm">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                              <tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>
									
									
									
									<tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        customeremail@email.com
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">X-RAY</span>
                                    </td>

                                    <td>
                                       22
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="view-files.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>View Files</a>
                                                <a class="dropdown-item" href="user-details.php"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>View Details</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2018-19 © Your 2nd Look
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

		
		
		
		
		
		

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		
		<!-- Data Tables  -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
		
		
        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		<script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
            });
        </script>

    </body>
</html>