<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Your 2nd Look - Support Center</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
		
		<!-- Dropzone css -->
         <link href="../plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" /> 
		
		<!-- DataTables -->
        <link href="../plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- Sweet Alert css -->
        <link href="../plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>

    <body>
		
		<?php include 'include/admin-header.php';?>

        


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                           
                            <h4 class="page-title">Supports</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title">Manage Tickets</h4>

                            <div class="row text-center">
                    <div class="col-sm-6 col-lg-6 col-xl-4">
                        <div class="card-box widget-flat border-custom bg-custom text-white">
                            <i class="fi-tag"></i>
                            <h3 class="m-b-10">258</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Total Support Sent</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-xl-4">
                        <div class="card-box bg-warning widget-flat border-warning text-white">
                            <i class="fi-archive"></i>
                            <h3 class="m-b-10">53</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Pending Support</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-xl-4">
                        <div class="card-box widget-flat border-success bg-success text-white">
                            <i class="fi-help"></i>
                            <h3 class="m-b-10">205</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Solved Support</p>
                        </div>
                    </div>
                    
                </div>


                            <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%" id="datatable">
                                <thead>
                                <tr>
                                    <th>
                                        ID
                                    </th>
                                    <th>Requested By</th>
                                    <th>Subject</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>Created Date</th>
                                    <th class="hidden-sm">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td><b>#1256</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">George A. Llanes</span>
                                        </a>
                                    </td>

                                    <td>
                                        Support for theme
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Low</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        2017/04/28
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#2542</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Jose D. Delacruz</span>
                                        </a>
                                    </td>

                                    <td>
                                        New submission on your website
                                    </td>

                                    <td>
                                        <span class="badge badge-warning">Medium</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Closed</span>
                                    </td>

                                    <td>
                                        2008/04/25
                                    </td>



                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                             <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#320</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Phyllis K. Maciel</span>
                                        </a>
                                    </td>

                                    <td>
                                        Verify your new email address!
                                    </td>

                                    <td>
                                        <span class="badge badge-danger">High</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        2017/04/20
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                             <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#1254</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Margeret V. Ligon</span>
                                        </a>
                                    </td>

                                    <td>
                                        Your application has been received!
                                    </td>

                                    <td>
                                        <span class="badge badge-danger">High</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Closed</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                             <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#1020</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Erwin E. Brown</span>
                                        </a>
                                    </td>

                                    <td>
                                        A new rating has been received
                                    </td>

                                    <td>
                                        <span class="badge badge-warning">Medium</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Closed</span>
                                    </td>

                                    <td>
                                        2013/08/11
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                             <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#854</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">William L. Trent</span>
                                        </a>
                                    </td>

                                    <td>
                                        Your Profile has been accepted
                                    </td>

                                    <td>
                                        <span class="badge badge-danger">High</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#9501</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Amy R. Barnaby</span>
                                        </a>
                                    </td>

                                    <td>
                                        Homeworth for your property increased
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Low</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                           <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#3652</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Jessica T. Phillips</span>
                                        </a>
                                    </td>

                                    <td>
                                        Item Support Message sent
                                    </td>

                                    <td>
                                        <span class="badge badge-warning">Medium</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Closed</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#9852</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Debra J. Wilson</span>
                                        </a>
                                    </td>

                                    <td>
                                        Your item has been updated!
                                    </td>

                                    <td>
                                        <span class="badge badge-danger">High</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                              <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#3652</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Luke J. Sain</span>
                                        </a>
                                    </td>

                                    <td>
                                        Your password has been reset
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Low</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                             <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#1352</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Karen R. Doyle</span>
                                        </a>
                                    </td>

                                    <td>
                                        Question regarding your Bootstrap Theme
                                    </td>

                                    <td>
                                        <span class="badge badge-danger">High</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#3562</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Freddie J. Plourde</span>
                                        </a>
                                    </td>

                                    <td>
                                        Security alert for my account
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Low</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#3658</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Darrell J. Cook</span>
                                        </a>

                                    </td>

                                    <td>
                                        Christopher S. Ahmad
                                    </td>

                                    <td>
                                        <span class="badge badge-warning">Medium</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Closed</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                             <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#2251</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Mark C. Diaz</span>
                                        </a>
                                    </td>

                                    <td>
                                        Verify your new email address!
                                    </td>

                                    <td>
                                        <span class="badge badge-danger">High</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                           <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>#3654</b></td>
                                    <td>
                                        <a href="javascript: void(0);"><span class="ml-2">Robert K. Joseph</span>
                                        </a>
                                    </td>

                                    <td>
                                        Support for theme
                                    </td>

                                    <td>
                                        <span class="badge badge-secondary">Low</span>
                                    </td>

                                    <td>
                                        <span class="badge badge-success">Open</span>
                                    </td>

                                    <td>
                                        01/04/2017
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                             <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="reply-ticket.php"><i class="mdi mdi-reply-all mr-2 text-muted font-18 vertical-middle"></i>Reply</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Solved</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Delete</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-priority"><i class="mdi mdi-tag-plus mr-2 font-18 text-muted vertical-middle"></i>Change Priority</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2018-19 © Your 2nd Look
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="change-priority" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Change Support Priority</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  
		 <label for="exampleFormControlSelect1">Current Status</label> 
		  <span class="badge badge-secondary">Low</span>
		  
       <div class="form-group">
    <label for="exampleFormControlSelect1">Select Priority Type</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Low</option>
      <option>High</option>
      <option>Medium</option>
      
      
    </select>
  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
		
		
		
		
		
		

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		
		<!-- Data Tables  -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
		
		<!-- Sweet Alert Js  -->
        <script src="../plugins/sweet-alert/sweetalert2.min.js"></script>
        <script src="assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		<script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
            });
        </script>

    </body>
</html>