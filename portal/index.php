<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Your 2nd Look - Customer Portal</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
		
		<!-- Dropzone css -->
         <link href="../plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" /> 
		
		<!-- Sweet Alert css -->
        <link href="../plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>

    <body>
		
		<?php include 'include/header.php';?>

        


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                           
                            <h4 class="page-title">MRI - Cardio</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <button type="button" class="btn btn-custom btn-rounded w-md waves-effect waves-light pull-right" data-toggle="modal" data-target="#upload"><i class="mdi mdi-upload"></i> Upload Images</button>
                            <h4 class="header-title m-b-30">My Files</h4>

                            <div class="row">
                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/jpg.svg" alt="icon">
                                        </div>
                                        <a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">invoice_project.pdf</h5>
                                            <p class="mb-0"><small>568.8 kb</small></p>
                                            <p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
											
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/png.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Bmpfile.bmp</h5>
                                            <p class="mb-0"><small>845.8 mb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/jpg.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Photoshop_file.ps</h5>
                                            <p class="mb-0"><small>684.8 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/png.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Avifile.avi</h5>
                                            <p class="mb-0"><small>5.9 mb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/gif.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Cadfile.cad</h5>
                                            <p class="mb-0"><small>95.8 mb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/png.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Mytextfile.txt</h5>
                                            <p class="mb-0"><small>568.8 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/jpg.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Epsfile.eps</h5>
                                            <p class="mb-0"><small>568.8 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/jpg.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Project_file.dll</h5>
                                            <p class="mb-0"><small>684.3 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/png.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">Website_file.sql</h5>
                                            <p class="mb-0"><small>457.8 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/jpg.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">invoice_project.pdf</h5>
                                            <p class="mb-0"><small>568.8 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/jpg.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">invoice_project.pdf</h5>
                                            <p class="mb-0"><small>568.8 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xl-2">
                                    <div class="file-man-box">
                                        <a href="" class="file-close"><i class="mdi mdi-close-circle"></i></a>
                                        <div class="file-img-box">
                                            <img src="assets/images/file_icons/png.svg" alt="icon">
                                        </div>
										<a href="#" class="file-comments" data-toggle="modal" data-target="#comments"><i class="mdi mdi-comment"></i> </a>
                                        <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a>
                                        <div class="file-man-title">
                                            <h5 class="mb-0 text-overflow">invoice_project.pdf</h5>
                                            <p class="mb-0"><small>568.8 kb</small></p>
											<p class="mb-0"><small><?php echo "Created date is " . date("d-m-Y h:i:sa");?></small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center mt-3">
								<button type="button" class="btn btn-danger btn-rounded waves-light waves-effect btn-lg" id="sa-imagesupload">SUBMIT FILES</button>
								
                                
                            </div>

                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2018-19 © Your 2nd Look
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
<!-- Modal -->
<div class="modal fade" id="comments" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Enter Comments</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <textarea class="form-control" rows="5" placeholder="Enter any comments if you want to share with this image"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
		
<!-- Modal -->
<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Upload Images of MRI - Cardio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-box">
                            <h4 class="header-title m-t-0">File Uploader</h4>
                            <p class="text-muted font-14 m-b-10">
                                You can upload miltiple files at once.
                            </p>

                            <form action="#" class="dropzone" id="dropzone">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>

                            </form>
                            <div class="clearfix text-right mt-3">
                                <button type="button" class="btn btn-custom waves-effect waves-light">Submit</button>
                            </div>

                        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		
		<!-- Dropzone js -->
        <script src="../plugins/dropzone/dropzone.js"></script>
		
		<!-- Sweet Alert Js  -->
        <script src="../plugins/sweet-alert/sweetalert2.min.js"></script>
        <script src="assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>